#### tufts_service_robots_webapi

This project implements a Flask API for a webserver to assist [Tufts Service Robots](https://github.com/jsinapov/tufts_service_robots). Please see the relevant documentation:

* [Installation](docs/installation.md) was inspired by [Flask Mega-Tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world), a useful reference during code development.
* Please see the [api_v1](docs/api_v1.md) documentation for usage from a turtlebot client.

