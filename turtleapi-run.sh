#!/bin/bash

if [ -z "TURTLEAPI_DIR" ] ; then
    export TURTLEAPI_DIR=/home/airlab/tufts_service_robots_webapi
fi

cd "$TURTLEAPI_DIR" || { echo "Failed cd $TURTLEAPI_DIR" ; exit 1 ; }

if [ -d venv ] ; then
    source venv/bin/activate || { echo "Failed source venv/bin/activate" ; exit 1 ; }
else
    python3 -m venv venv || { echo "Failed python3 -m venv venv" ; exit 1 ; }
    source venv/bin/activate || { echo "Failed source venv/bin/activate" ; exit 1 ; }
    pip3 install --upgrade pip || { echo "Failed pip3 install --upgrade pip" ; exit 1 ; }
fi

pip3 install -r requirements.txt || { echo "Failed pip3 install -r requirements.txt" ; exit 1 ; }

export FLASK_APP=turtleapi_app.py
export FLASK_ENV=development
export FLASK_DEBUG=1
./$FLASK_APP || { echo "Failed $FLASK_APP" ; exit 1 ; }
