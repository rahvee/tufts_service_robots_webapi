from flask import Flask
import os
import yaml

app = Flask(__name__)

yaml_config_filename = 'config.yaml'


# function will confine scope of variables, so they don't appear globally in this module
def _initialize():
    if not os.path.isfile(yaml_config_filename):
        raise RuntimeError(
            "Error: {} not found. Please copy and edit example_config.yaml.".format(yaml_config_filename))

    with open(yaml_config_filename, "r") as f:
        yaml_config = yaml.load(f.read())

    for key in yaml_config:
        app.config[key] = yaml_config[key]

    if 'UPLOADS_DIR' not in app.config:
        raise RuntimeError("Error: UPLOADS_DIR not found in config.yaml. Please copy and edit example_config.yaml.")

    if not os.path.isdir(app.config['UPLOADS_DIR']):
        raise RuntimeError("Error: UPLOADS_DIR is not a dir. Please edit config.yaml or create directory.")

    if 'ACCESS_TOKENS' not in app.config or not isinstance(app.config['ACCESS_TOKENS'], list):
        raise RuntimeError("Error: ACCESS_TOKENS not found in config.yaml. Please copy and edit example_config.yaml.")

    rosbags_dir = os.path.join(app.config['UPLOADS_DIR'], 'rosbags')
    os.makedirs(rosbags_dir, exist_ok=True)
    if not os.path.isdir(rosbags_dir):
        raise RuntimeError("Error: Failed to make dir {}.".format(rosbags_dir))


_initialize()

from turtleapi_app.api_v1 import bp as api_bp
app.register_blueprint(api_bp, url_prefix='/api/v1.0')

from turtleapi_app import routes
