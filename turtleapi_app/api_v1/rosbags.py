from turtleapi_app.api_v1 import bp
from werkzeug.utils import secure_filename
from flask import request
from flask import redirect
from flask import jsonify
from flask import Response
from turtleapi_app import app

import hashlib
import os


def sha256_sum(file_path: str) -> str:
    sha256 = hashlib.sha256()
    with open(file_path, 'rb') as f:
        chunk_size = 8192
        while True:
            chunk = f.read(chunk_size)
            if len(chunk) == 0:
                break
            sha256.update(chunk)
    return sha256.hexdigest().lower()  # lower() just in case.


@bp.route('/rosbags/<string:filename>/sha256sum', methods=['GET'])
def get_rosbag_sha256(filename):
    try:
        access_token = request.args.get('access_token', default='')
        if access_token not in app.config['ACCESS_TOKENS']:
            r = jsonify()  # type: Response
            r.status_code = 401  # Setting status_code to 401 automatically sets status to "UNAUTHORIZED"
            return r

        if filename != secure_filename(filename):
            r = jsonify({'message': 'not secure filename'})  # type: Response
            r.status_code = 400  # Setting status_code to 400 automatically sets status to "BAD REQUEST"
            return r
        if not filename.endswith('.bag'):
            r = jsonify({'message': 'not .bag file'})  # type: Response
            r.status_code = 400  # Setting status_code to 400 automatically sets status to "BAD REQUEST"
            return r

        rosbags_dir = os.path.join(app.config['UPLOADS_DIR'], 'rosbags')
        file_path = os.path.join(rosbags_dir, filename)
        if not os.path.isfile(file_path):
            r = jsonify()  # type: Response
            r.status_code = 404  # Setting status_code to 404 automatically sets status to "NOT FOUND"
            return r

        sha256sum = sha256_sum(file_path)

        r = jsonify({'sha256sum': sha256sum})  # type: Response
        r.status_code = 200  # Setting status_code to 201 automatically sets status to "OK"
        return r
    except Exception as e:
        app.logger.error('upload_rosbag: Caught unhandled exception {}'.format(e))
        r = jsonify()  # type: Response
        r.status_code = 500  # Setting status_code to 500 automatically sets status to "INTERNAL SERVER ERROR"
        return r


@bp.route('/rosbags/<string:filename>', methods=['POST'])
def upload_rosbag(filename):  # type: (str) -> Response
    try:
        access_token = request.args.get('access_token', default='')
        if access_token not in app.config['ACCESS_TOKENS']:
            r = jsonify()  # type: Response
            r.status_code = 401  # Setting status_code to 401 automatically sets status to "UNAUTHORIZED"
            return r

        if filename != secure_filename(filename):
            r = jsonify({'message': 'not secure filename'})  # type: Response
            r.status_code = 400  # Setting status_code to 400 automatically sets status to "BAD REQUEST"
            return r
        if not filename.endswith('.bag'):
            r = jsonify({'message': 'not .bag file'})  # type: Response
            r.status_code = 400  # Setting status_code to 400 automatically sets status to "BAD REQUEST"
            return r

        expected_hash = request.args.get('sha256sum', default='')

        rosbags_dir = os.path.join(app.config['UPLOADS_DIR'], 'rosbags')
        file_path = os.path.join(rosbags_dir, filename)
        with open(file_path, "wb") as f:
            sha256 = hashlib.sha256()
            chunk_size = 4096
            while True:
                chunk = request.stream.read(chunk_size)
                if len(chunk) == 0:
                    break
                f.write(chunk)
                sha256.update(chunk)
            hash_result = sha256.hexdigest().lower()  # lower() just in case

        if hash_result == expected_hash:
            r = jsonify()  # type: Response
            r.status_code = 201  # Setting status_code to 201 automatically sets status to "CREATED"
            return r
        else:
            os.remove(file_path)
            r = jsonify({'message': 'sha256sum mismatch'})  # type: Response
            r.status_code = 409  # Setting status_code to 409 automatically sets status to "CONFLICT"
            return r
    except Exception as e:
        app.logger.error('upload_rosbag: Caught unhandled exception {}'.format(e))
        r = jsonify()  # type: Response
        r.status_code = 500  # Setting status_code to 500 automatically sets status to "INTERNAL SERVER ERROR"
        return r
