#!/usr/bin/env python3
import sys
import os
import yaml
from turtleapi_app import app


if __name__ == '__main__':
    app.run()
