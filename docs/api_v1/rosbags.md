## rosbags api_v1

### GET sha256sum for existing file

`GET /api/v1.0/rosbags/<string:filename>/sha256sum`

  filename must end with `.bag`

  This method can be used to check for the existence of a file before attempting to upload the same file.

  - Parameters:
    - `access_token` (Required): The secret token used to authorized the request.

  Return:

  - 200 OK: The file sha256sum is returned.
  - 400 BAD REQUEST: The filename was not properly formatted.
  - 401 UNAUTHORIZED: The access_token was not authorized.
  - 500 INTERNAL SERVER ERROR: The server encountered an unexpected error.

### POST upload new file

`POST /api/v1.0/rosbags/<string:filename>`

  filename must end with `.bag`
  
  The rosbag specified by `filename` can be stream uploaded, and will be saved in the `rosbags` subdirectory, in the `UPLOADS_DIR` specified by `config.yaml`

  * Parameters:
    * `access_token` (Required): The secret token used to authorized the request.
    * `sha256sum` (Required): The lowercase sha256 hex digest of the file being uploaded. If the file received by the server does not match the `sha256sum`, the server will reject the file and return 409 CONFLICT

  Return:

  * 201 CREATED: The file was successfully uploaded and sha256sum matched.
  * 400 BAD REQUEST: The filename was not properly formatted.
  * 401 UNAUTHORIZED: The access_token was not authorized.
  * 409 CONFLICT: A sha256 mismatch occurred. Try again.
  * 500 INTERNAL SERVER ERROR: The server encountered an unexpected error.