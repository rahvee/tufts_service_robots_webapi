#### Tufts Service Robots Webapi Installation



#### ssh server

1. Install sshd

        sudo apt-get -y install openssh-server

2. Harden sshd

        sudo vi /etc/ssh/sshd_config

   1. Disable all authentication techniques except pubkey.

            PubkeyAuthentication yes
            KbdInteractiveAuthentication no
            RSAAuthentication no
            RhostsRSAAuthentication no
            HostbasedAuthentication no
            ChallengeResponseAuthentication no
            PasswordAuthentication no
            KerberosAuthentication no
            GSSAPIAuthentication no

   2. And restart sshd.

            sudo systemctl restart sshd

   3. On clients, always use RSA, with either 2048 or 4096 bit keys, and a descriptive comment that uniquely identifies the client.

      1. For example, on mac or linux:

                ssh-keygen -t rsa -b 2048 -C "jdoe@jdoelaptop"

      2. On windows, use `puttygen` and make sure you select RSA 2048 or 4096.

   4. Install ssh public keys to `.ssh/authorized_keys` file on the server. Directory perms must be 700 and file perms must be 600 or else sshd will not use them (sshd uses `StrictModes`)



#### Flask API server installation and run

1. System requirements:

        sudo apt-get -y install git python3 python3-pip python3-venv

2. This is currently written assuming `airlab` user with home dir `/home/airlab`

3. Become the `airlab` user and checkout code:

        git clone https://gitlab.com/rahvee/tufts_service_robots_webapi.git

4. Copy and edit the config:

        cd tufts_service_robots_webapi
        cp example_config.yaml config.yaml
        vi config.yaml

5. Now run it:

        ./turtleapi-run.sh

6. It should display "Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)".

7. Unfortunately, running as described above will kill your flask service when you quit or disconnect your terminal. If you wish to leave the service running, even while you disconnect your ssh client or terminal window, you can do it like this:

        nohup ./turtleapi-run.sh &


#### nginx configuration

1. Install nginx:

        sudo apt-get -y install nginx
        cd /etc/nginx/sites-available
        sudo cp -p default default.orig
        sudo vi default
   
2. Comment out these lines:

        # listen 80 default_server;
        # listen [::]:80 default_server;

3. Un-comment this line:

        listen 443 ssl default_server;

4. Below the snakeoil line, add these:

        # include snippets/snakeoil.conf;
        ssl_certificate /etc/ssl/certs/turtlecloud.eecs.tufts.edu-Jun-15-2020.chained.crt;
        ssl_certificate_key /etc/ssl/private/turtlecloud.eecs.tufts.edu-Jun-15-2020-private.key;
        ssl_protocols       TLSv1.1 TLSv1.2;
        ssl_ciphers         HIGH:!aNULL:!MD5;
        keepalive_timeout   70;
        client_max_body_size 10G;

5. Above the existing `server` section, add a whole new `server` section, which will redirect all port 80 requests to 443:

        server {
            # listen on port 80 (http)
            listen 80;
            server_name _;
            location / {
                # redirect any requests to the same URL but on https
                return 301 https://$host$request_uri;
            }
        }

5. To generate the SSL cert:

        sudo su -
        mkdir turtlecloud_cert
        cd turtlecloud_cert
        requestname='turtlecloud.eecs.tufts.edu'
        openssl req -newkey rsa:2048 -nodes -sha256 -keyout turtlecloud.eecs.tufts.edu-Jun-15-2020-private.key -out turtlecloud.eecs.tufts.edu-Jun-15-2020.csr -subj "/C=US/ST=Massachusetts/L=Medford/O=Tufts Technology Services/CN=${requestname}/emailAddress=ess@tufts.edu"

    1. Work with Tufts IT staff (email staff at eecs.tufts.edu) to obtain the signed certificate and chain (bundle). Then finish assembling it and installing the files to the right locations as follows:

            cat turtlecloud.eecs.tufts.edu-Jun-15-2020.crt InCommon_chain.crt > turtlecloud.eecs.tufts.edu-Jun-15-2020.chained.crt
            cp turtlecloud.eecs.tufts.edu-Jun-15-2020.chained.crt /etc/ssl/certs/
            cp turtlecloud.eecs.tufts.edu-Jun-15-2020-private.key /etc/ssl/private/

6. Now you should be able to test the https connection.

        systemctl restart nginx
   
    1. Browse to [https://turtlecloud.eecs.tufts.edu/](https://turtlecloud.eecs.tufts.edu/) and ensure you have a proper Secure connection.

7. And finally, we need to configure nginx to run the flask application.
    1. Please note: This is the non-production way of doing things. It's good enough for our experimental and development work, which is lightweight and only locally exposed, but not good enough for a real live public facing server. To deploy a production server, see the official flask [Deployment Guide](http://flask.pocoo.org/docs/1.0/deploying/) which we're not doing.
    2. Once again, edit `/etc/nginx/sites-available/default`
        3. Remove the `root`, `index`, `server_name`, and `location /`. At this point, there's nothing remaining from the ubuntu-provided `server` block; everything in there has been provided by us. Add this:

                server_name _;
                
                location / {
                    # forward application requests to flask
                    proxy_pass http://127.0.0.1:5000;
                    proxy_redirect off;
                    proxy_set_header Host $host;
                    proxy_set_header X-Real-IP $remote_addr;
                    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                }
                
                location /static {
                    # handle static files directly, without forwarding to the application
                    alias /home/airlab/tufts_service_robots_webapi/turtleapi_app/static;
                    expires 30d;
                }

#### To debug in PyCharm

1. Open project directory
2. Add a new python3 virtual environment as your project interpreter
3. Go to Run > Edit Configurations
  1. Click [+] to add a configuration. Choose Flask server.
  2. Target type: Script path
  3. Script: browse to the `turtleapi_app.py` file
  4. Optionally, give it a new name like `Flask (turtleapi_app.py)`
4. From the main window, choose Run > Debug, and select your newly created Flask configuration.
